import React from "react";
import { Result, Button } from "antd";
import { useHistory } from "react-router-dom";

export default () => {
  const history = useHistory();
  return (
    <Result
      status="404"
      title="404"
      subTitle="Уучлаарай!, Хүсэлт олдсонгүй."
      extra={<Button type="primary" onClick={() => history.push("/")}>Нүүр хуудас</Button>}
    />
  );
};
