import { FundProjectionScreenOutlined, SafetyOutlined, ScanOutlined, UserOutlined, UserSwitchOutlined } from "@ant-design/icons";
import { Avatar, Card, Col, Row, Space } from "antd";
import React from "react";
import styled from "styled-components";
import { PageContent, PageHeader } from "../../components/Layout";  
import Cover from "./components/Cover";
import Footer from "./components/Footer";
import sample from '../../assets/mask.webm';
import Info from "./components/Info";

const { Meta } = Card;
const Home = () => {

  return (
    <Container> 
    <Cover />
      <Banner>  
      </Banner> 
      <div className="container" style={{ marginTop: "40px" }}>
        <div className="what-is container">
          <Row gutter={[16,16]}  style={{ padding: "12px" }}>
            <Col xs={24} sm={24} md={14}> 
              <h1>Царай таних систем гэж юу вэ?</h1>
              <p>
              Царай таних систем гэдэг нь видео бичлэг дээр суурилсан хүний царайг таньж, тогтоох чадвартай программ хангамж юм. Энэхүү технологи нь дижитал бичлэг эсвэл зургаас хүний царайг танин, хэлбэр хэмжээ, сүүдэр зэргийг нь харьцуулж, анализ хийсний үр дүнд тодорхой хэмжээний мэдээлэл цуглуулах, түүнийгээ нэгтгээд, дата бааз үүсгэдэг биометрийн программ хангамж юм.

              <br />
              <br />
              Ингэснээр тодорхой нэг түлхүүр үгээр хайлт хийж болдог. Жишээ нь: Хэрвээ та өчигдөр танай байгууллагад нэвтэрсэн сахалтай нүдний шилтэй хүний бичлэгийг шүүж үзэхийг хүсвэл цаг эсвэл байршлын мэдээллийг оруулахгүйгээр "Сахалтай", "нүдний шилтэй" гэсэн түлхүүр үгээр хайлт хийхэд тухайн хүний бичлэг гарч ирнэ.
              </p>
            </Col>
            <Col xs={24} sm={24} md={10}> 
              <StyledVideo>
                <video className='videoTag' autoPlay loop muted>
                    <source src={sample} type='video/mp4' />
                </video>
              </StyledVideo>
            </Col>
          </Row>
        </div>
      </div>
      <div className="container" style={{ marginTop: "40px" }}>
        
        <div className="can-use-title">
          Царай таних системийн ашиглагдах боломжууд
        </div>
        <StyledUse>

          <div className="items">
            <div className="icon-wrapper">
            <ScanOutlined />
            </div>

            <div className="little-card-title">
            Face-recognition (FRD)
            </div>
            <div className="little-card-description">
            Face-recognition нь хүмүүсийн нүүр царайг сканнердаж, мэдээллийг бүртгэгдсэн өгөгдөлтэй харьцуулан боловсруулж, үр дүнг хэдхэн минутын дотор анализ хийж буцаан авах боломжтой систем юм.
            </div>
          </div>
          
           <div className="items">
            <div className="icon-wrapper">
            <UserSwitchOutlined />
            </div>

            <div className="little-card-title">
              Emotional generator (EQ)
            </div>
            <div className="little-card-description">
              Энэхүү системийн тусламжтайгаар хүмүүсийн нүүр царайг сканнердаж царайны хувьралыг ямар байгааг тодорхойлох үйлдэлийг автоматжуулах боломжтой юм. 
            </div>
          </div>
          
           <div className="items">
            <div className="icon-wrapper">
            <FundProjectionScreenOutlined />
            </div>

            <div className="little-card-title">
              Machine learning algorithms (neural network)
            </div>
            <div className="little-card-description">
              Энэхүү систем нь бүртгэгдсэн өгөгдөлийн тусламжтай өөрөө суралцах хиймэл оюун ухаанаар ажилдаг тул өгөгдөл оруулах тусам үнэн зөв, нарийвчлалтай өгөгдлийг танд өгөх болно. 
            </div>
          </div>
        </StyledUse>
      </div>
      {/* <Intro/> */}
      {/* <Footer />  */}

      <Info />
    </Container>
  );
};

const StyledUse = styled.div`
  display: flex;
  align-items: stretch;
  gap: 30px;

  .items {
    flex-grow: 1; 
    border-radius: 8px;
    padding: 24px;
    border: 1px solid #f7f7f7;
    min-width: 300px; 
    box-shadow: 0 5px 5px 0 rgb(154 160 185 / 5%), 0 5px 30px 0 rgb(166 173 201 / 22%);

    .icon-wrapper { 
      display: flex;
      justify-content: center;
      align-items: center;
      font-size: 50px;
      padding: 24px;
    }
  } 
  
  @media (max-width: 1000px) {  
    flex-wrap: wrap;
  }

`;

const StyledVideo = styled.div` 
  height: 100%;
  display: flex;
  justify-content: end;
  align-items: center;  
  
  video { 
    width: 100%;
    max-width: 500px;
    border-radius: 10px;
  }
`;


const Banner = styled.div`   
  min-height: 330px;
  padding: 24px;
  width: 100%;
  background: rgb(129,172,252);
  background: linear-gradient(100deg, rgba(129,172,252,1) 12%, rgba(62,128,255,1) 81%);
  align-items: center;
  display: flex;
  justify-content: center;
`;


const Container = styled.div`  

  .little-card-title {
    margin-bottom: 8px;
    color: var(--text);
    font-weight: 600;
    font-size: 24px;
    font-family: Inter,sans-serif;
    font-style: normal;
    line-height: 36px;
    letter-spacing: 0;
    text-align: center;
  }

  .little-card-description {
    position: relative;
    font-weight: 400;
    font-size: 16px;
    font-family: Inter,sans-serif;
    font-style: normal;
    line-height: 28px;
    letter-spacing: 0;
    text-align: center; 
  }
  .can-use-title {
    max-width: 600px;
    white-space: normal;
    margin: auto;
    padding: 100px 0px 40px 0px;
    font-weight: 600;
    font-size: 48px;
    font-family: Inter,sans-serif;
    font-style: normal;
    line-height: 60px;
    letter-spacing: 0;
    text-align: center;

  }
  .card-title { 
    padding: 14px 0;
    color: #333c4d;
    font-weight: 600;
    font-size: 20px;
    font-family: Inter,sans-serif;
    font-style: normal;
    line-height: 32px;
    letter-spacing: 0;
    text-align: left;
    
  }
  .card-text {  
    max-width: 600px;
    color: #333c4d;
    font-weight: 400;
    font-size: 16px;
    font-family: Inter,sans-serif;
    font-style: normal;
    line-height: 28px;
    letter-spacing: 0;
    text-align: left;
  }
  
  .container { 
    padding: 45px 0;
    max-width: 1280px; 
    width: 100%;
    margin: auto; 
  }

  .what-is {
    p { 
      font-weight: 400;
      font-size: 20px;
      line-height: 32px;
    }
  }
`;

const Content = styled.div`
  max-width: 1280px; 
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: stretch;
  gap: 20px;


  .card { 
    background-color: #fff; 
    padding: 20px;
    border-radius: 5px;
    max-width: 600px;

    .card-title {
      
    }
  }

`;

export default Home;