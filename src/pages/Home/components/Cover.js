import { Col, Row } from "antd";
import React from "react";
import styled from "styled-components";  
import sample from '../../../assets/live.webm';


const Cover = () => {

  return (
    <Container>
      <Row gutter={[16,16]} style={{ maxWidth: "1280px",  margin: "0 auto", padding: "12px 12px"}} >
        <Col xs={24} sm={24} md={14}> 
          <LeftWrapper>
          <TextStyled> 
            <div className="tagged-text">
              Хяналт, видео аналитикийн шийдэл
            </div>
            <div className="big-title">
              ХИЙМЭЛ ОЮУН УХААНААР АЖИЛЛАДАГ ЦАРАЙ ТАНИХ СИСТЕМ
            </div>
            <p className="description">
              царай таних системтэй уялдан ажиллах боломжтой хамгийн төгс шийдэл бүхий технологиудтай нэгдэн ажиллах боломжтой.
            </p>
          </TextStyled>
        </LeftWrapper>
        </Col>
        <Col xs={24} sm={24} md={10}> 
          <StyledVideo>
            <video className='videoTag' autoPlay loop muted>
                <source src={sample} type='video/mp4' />
            </video>
          </StyledVideo>
        </Col>
      </Row>
    </Container>
  );
};

const LeftWrapper = styled.div ` 
  display: flex;
  justify-content: start;
  align-items: center;
  height: 100%;
`;

const TextStyled = styled.div`
  max-width: 600px; 
  .description { 
    margin-top: 20px;
    max-width: 600px;
    margin-bottom: 45px;
    color: #333c4d;
    font-size: 20px;
    line-height: 32px; 
    font-family: Inter,sans-serif; 
    font-weight: 500; 
    font-style: normal; 
    letter-spacing: 0; 
  }
  .tagged-text { 
    display: inline-block;
    margin: 64px 0 24px;
    padding: 4px 8px;
    font-weight: 600;
    font-size: 16px;
    line-height: 28px;
    background: #e5eeff;
    border-radius: 4px;

  } 
  .big-title { 
    font-size: 48px;
    max-width: 600px;
    color: #080A1A;
    font-weight: 500;
    line-height: 60px; 
  }
`

const StyledVideo = styled.div` 
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;  
  video { 
    width: 100%;
  background: rgb(249,247,255);
  }
  background: rgb(249,247,255);
`;

const Container = styled.div` 
  height: auto; 
  display: flex;
  align-items: center;
  background: rgb(249,247,255);
  background: linear-gradient(100deg, rgba(249,247,255,1) 0%, rgba(249,247,255,1) 100%);


`; 

export default Cover;