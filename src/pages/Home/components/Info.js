import { Avatar, Button, Carousel, Col, Divider, Image, Row } from "antd";
import React, { useRef } from "react";
import styled from "styled-components";  
import one from '../../../assets/1.svg';
import two from '../../../assets/2.svg';
import three from '../../../assets/3.svg';
import four from '../../../assets/4.svg'; 
import set from '../../../assets/fallback.png'; 
import imac from '../../../assets/imac.png'; 
import LisaFace from "../../../assets/lisaface.jpeg"
import deckgif from "../../../assets/deck.gif"
import facemove from "../../../assets/facemove.webp"


const Info = () => { 
  return (
    <div> 
      <Container style={{ backgroundImage: `url(${one})` }}> 
        <div className="container">
          <div className="card"> 
            <Row align="middle">
            <Col  sm={24} md={12}> 
              <div className="image-wrapper">
                <Image src={deckgif} preview={false} />
              </div>
              </Col>
            <Col  sm={24} md={12}> 
              <div className="description">
                <div className="label">
                Asset Library
                </div>
                <h2 className="title">
                Customize to your brasnd in seconds and grab attention
                </h2>
                <div className="desc">
s
              lorem
                </div>
              </div>
              </Col>
            </Row>
          </div>
        </div> 
      </Container>


      <Container bg={{one : one, two : two, three : three, four : four }} style={{ backgroundImage: `url(${two})` , backgroundPosition: "right" }} > 
        <div className="container">
          <div className="card"> 
            <Row align="middle">
              <Col  sm={24} md={12}> 
              <div className="description">
                <div className="label">
                Asset Library
                </div>
                <h2 className="title">
                Customize to your brand in seconds and grab attention
                </h2>
                <div className="desc">
                Drag and drop your logo or a screenshot of your website to auto-magically extract your brand colors. Do you want to use your company’s fonts? No problem. Your custom color palette and uploaded fonts will always be accessible in the online poster maker. Event posters, announcement or informative posters, and any business posters – in the comfort of your web browser.
                </div>
              </div>
              </Col>
              <Col  sm={24} md={12}> 
              <div className="image-wrapper">
                <Image src={facemove} preview={false} />
              </div>
              </Col>
            </Row>
          </div>
        </div> 
      </Container>

      <Container bg={{one : one, two : two, three : three, four : four }} style={{ backgroundImage: `url(${three})` , backgroundPosition: "center" }} > 
        <div className="container">
          <div className="card"> 
            <Row align="middle">
              <Col  sm={24} md={12}> 
              <div className="image-wrapper">
                <Image src={LisaFace} preview={false} />
              </div>
              </Col>
              <Col  sm={24} md={12}> 
              <div className="description">
                <div className="label">
                Asset Library
                </div>
                <h2 className="title">
                Customize to your brand in seconds and grab attention
                </h2>
                <div className="desc">
                Drag and drop your logo or a screenshot of your website to auto-magically extract your brand colors. Do you want to use your company’s fonts? No problem. Your custom color palette and uploaded fonts will always be accessible in the online poster maker. Event posters, announcement or informative posters, and any business posters – in the comfort of your web browser.
                </div>
              </div>
              </Col>
            </Row>
          </div>
        </div> 
      </Container>

      <Container bg={{one : one, two : two, three : three, four : four }} style={{ backgroundImage: `url(${four})` , backgroundPosition: "left" }} > 
        <div className="container">
          <div className="card"> 
            <Row align="middle" justify="center">
              <Col span={24} > 
              <div className="mac-image-wrapper">
                <Image src={imac} preview={false} />
              </div>
              </Col> 
              <Col span={24}> 
                <Row justify="center">
                  <Button type="primary" shape="round" size="large" style={{ background: "#00B8D4", borderColor: "#00B8D4" }} >Download now</Button>
                </Row>
                <Divider />
              </Col>
            </Row>
          </div>
        </div> 
      </Container>
    </div>
  );
};

const Container = styled.div`   
  background-repeat: no-repeat; 
  font-family: "Medium",sans-serif;


  .mac-image-wrapper { 

    padding: 24px;
      img {  
        border-radius: 40px !important;

      } 
  }
  .label { 
    color: #0fb4bb;
  }

  .title {
    color: #3c4c52;
    margin-bottom: 20px;
    font-size: 24px;
    letter-spacing: -.32px;
  }
  .desc {
    color: #637379;
    line-height: 1.5;
    font-family: "Medium",sans-serif;
    font-size: 16px;
    margin-bottom: 40px;
  }

  .card {
    .description { 
      padding: 24px;
      
    }
    .image-wrapper {
      padding: 24px;
      img {  
        border-radius: 40px !important;
        border: 1px solid #dddddd; 
      }
    }
  }
`; 

export default Info;
  // background-image: url(${props => props.bg.one}), url(${props => props.bg.two}), url(${props => props.bg.three}), url(${props => props.bg.four});