import { Avatar, Button, Carousel, Col, Image, Row } from "antd";
import React, { useRef } from "react";
import styled from "styled-components";  
import img1 from '../../../assets/set.png';
import img2 from '../../../assets/lisa.png';


const Intro = () => {

    const carousel = useRef() 
    // background: 'rgb(129,172,252)',
    // background: 'linear-gradient(100deg, rgba(129,172,252,1) 12%, rgba(62,128,255,1) 81%)'  

  return (
    <Container>
        <Button onClick={()=>carousel.current.next()}>Next</Button>
        <Carousel ref={carousel} > 
        <div>
            <div className="content-wrapper">
                <div className="content">
                    <Row>
                        <Col span={12}  >
                            <Image preview={false} src={img2} />
                        </Col>
                        <Col span={12}>
                        
                        </Col>
                    </Row>
                </div>
            </div> 
        </div>
        <div>
            <div className="content-wrapper">
                <div className="content">
                    <Row>
                        <Col span={12}  >
                            <Image preview={false} src={img1} />
                        </Col>
                        <Col span={12}>
                            
                        </Col>
                    </Row>
                </div>
            </div> 
        </div>
        </Carousel>
    </Container>
  );
};

const Container = styled.div`  
  /* background: rgb(249,247,255);
  background: linear-gradient(100deg, rgba(249,247,255,1) 0%, rgba(249,247,255,1) 100%); */

  .content-wrapper {
      height: 600px; 
      display: flex;
      justify-content: center;
      align-items: center;

      .img {
            height: 100%;
            width: 700px; 
        }
      .content {
        height: 500px; 
        width: 1280px;
        border-radius: 8px;
        padding: 24px;
        border: 1px solid #f7f7f7;
        min-width: 300px; 
        box-shadow: 0 5px 5px 0 rgb(154 160 185 / 5%), 0 5px 30px 0 rgb(166 173 201 / 22%);


        img { 
            max-height: 400px;
            object-fit: cover;
        }
      }
      
  }

`; 

export default Intro;