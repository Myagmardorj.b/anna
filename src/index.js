import { ConfigProvider } from "antd";
import mnMN from "antd/es/locale/mn_MN";
import moment from "moment";
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter as Router } from "react-router-dom";
import { PersistGate } from "redux-persist/integration/react";
import App from "./App";
import configureStore from "./store";
import "./styles/base.css";
import "./utils/momentLocale";

moment.locale("mn-MN");
const { store, persistor } = configureStore();

ReactDOM.render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <ConfigProvider locale={mnMN}>
        <Router>
          <App />
        </Router>
      </ConfigProvider>
    </PersistGate>
  </Provider>,
  document.getElementById("root")
);

export { store };
