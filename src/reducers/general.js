const initialState = {
  s3host        : undefined,
  userRoles     : [],
  postStatus    : [],
  rolePermission: [],
  isMobile      : false,
  drawer        : false,
  collapse      : false,
  newPost       : undefined,
  userStatus    : [],
  targetTypes   : []
};

const general = (state = initialState, action = {}) => {
  switch (action.type) {
    case "general.init": {
      const { s3host, postStatus = [], rolePermission = [], userRoles = [], newPost, userStatus = [], actionTypes =[], objectTypes=[], targetTypes = [] } = action.payload;
      return {
        ...state,
        s3host,
        userRoles,
        postStatus,
        rolePermission,
        newPost,
        userStatus,
        actionTypes,
        objectTypes,
        targetTypes
      };
    }
    case "general.is_mobile": {
      return {
        ...state,
        isMobile: action.payload,
      };
    }
    case "general.collapse": {
      return {
        ...state,
        collapse: action.payload,
      };
    }
    case "general.drawer": {
      return {
        ...state,
        drawer: action.payload,
      };
    }
    default:
      return state;
  }
};

export default general;
