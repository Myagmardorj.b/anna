import request from "../utils/request";

export const list = (data) => request.get("/api/logs", data);
export const login = (data) => request.post("/api/auth/login", data);
export const history = (data) => request.get("/api/logs", { ...data, object_type: data.object_type });
export const session = (data) => request.get("/api/logs/session", data);
export const historyTemplate = (data) => request.get("/api/logs/history/template", data);
