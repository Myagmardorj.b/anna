import * as app from "./app";
import * as audit from "./audit";
import * as auth from "./auth";
import * as dashboard from "./dashboard";
import * as general from "./general";
import * as notify from "./notify";
import * as permission from "./permission";
import * as post from "./post";
import * as sector from "./sector";
import * as staff from "./staff";
import * as user from "./user";


export {
  auth,
  general,
  permission,

  post,
  user,
  dashboard,
  sector,
  app,
  staff,
  audit,
  notify
};
