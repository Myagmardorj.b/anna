import request from "../utils/request";

export const userChart = (id) => request.get("/api/dashboard");
export const postChart = (id) => request.get("/api/dashboard/post");