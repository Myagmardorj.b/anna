import request from "../utils/request";

export const get = (data, options) =>
  request.get("/api/permission", data, options);

export const save = (data) =>
  request.post("/api/permission", data);
