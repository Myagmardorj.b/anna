import request from "../utils/request";

export const get = (id, options) =>
  request.get(`/api/staff/${id}`, null, options);

export const depo = (id, type) =>
  request.put(`/api/staff/${id}/depo`, type);

export const select = (data, options) =>
  request.get("/api/staff/select", data, options);

export const list = (data) =>
  request.get("/api/staff", data);

export const create = (data) =>
  request.post("/api/staff", data);

export const update = (id, data) =>
  request.put(`/api/staff/${id}`, data);

export const remove = (id) =>
  request.del(`/api/staff/${id}`);