import request from "../utils/request";

export const select = () => request.get("/api/category");
export const get = (id) => request.get(`/api/category/${id}`);
export const remove = (id) => request.del(`/api/category/${id}`);
export const update = (id, data) => request.put(`/api/category/${id}`, data);
export const create = (data) => request.post("/api/category/", data);
export const query = (data, options)=> request.get("/api/category/query", data, options);
