import request from "../utils/request";

export const get = (id, options) =>
  request.get(`/api/user/${id}`, null, options);

export const depo = (id, type) =>
  request.put(`/api/user/${id}/depo`, type);

export const select = (data, options) =>
  request.get("/api/user/select", data, options);

export const list = (data) =>
  request.get("/api/user", data);

export const create = (data) =>
  request.post("/api/user", data);

export const update = (id, data) =>
  request.put(`/api/user/${id}`, data);

export const remove = (id) =>
  request.del(`/api/user/${id}`);

export const password = (id, data) =>
  request.put(`/api/user/${id}/password`, data);