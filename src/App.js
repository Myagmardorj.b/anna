import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Route, Switch } from "react-router-dom";
import styled from "styled-components";
import { auth, general } from "./apis";
import { Header, Loader, PrivateRoute, PublicRoute } from "./components";
import { Login, Notfound } from "./pages"; 
import Home from "./pages/Home";    

const App = () => {
  const dispatch = useDispatch();
  const { token } = useSelector(state => state.auth);
  const [loading, setLoading] = React.useState(true);
  const { isMobile, collapse, drawer } = useSelector(state => state.general);

  React.useEffect(() => {
    const asyncFn = async () => {
      try {
        await Promise.all([general.init()(dispatch), auth.me()(dispatch)]);
        setLoading(false);
      } catch (e) {
        setLoading(false);
      }
    };

    if (token) {
      asyncFn();
    } else {
      setLoading(false);
    }
  }, [dispatch, token]);

  const updateDimensions = () => {
    const width = window.innerWidth;

    dispatch({
      type   : "general.is_mobile",
      payload: width <= 991
    });
  };

  useEffect(() => {
    updateDimensions();
    window.addEventListener("resize", updateDimensions);
    return () =>
      window.removeEventListener("resize", updateDimensions);
  }, []);

  if (loading) return <div><Loader/></div>;

  return (
    <div>
      <Switch>
        <PublicRoute path="/home" component={Home} /> 
        <PublicRoute path="/login" component={Login} /> 
        <PrivateRoute path="/">
          <Header />
          <Container >
            <Switch>
              <Route path="/" component={Home} exact />

              <Route component={Notfound} />
            </Switch> 
          </Container>
        </PrivateRoute>
      </Switch>
    </div>
  );
};

const Container = styled.div` 
  .ant-btn-primary { 
    background: #00B8D4;
  }

`;

export default App;
