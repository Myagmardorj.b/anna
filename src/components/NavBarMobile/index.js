import React, { useState } from "react";
import { Badge, Menu, Space } from "antd";
import { NavLink, useHistory } from "react-router-dom";
import { TeamOutlined, PieChartOutlined, MedicineBoxOutlined, FormOutlined, UserOutlined, SettingOutlined, FileProtectOutlined, HomeOutlined } from "@ant-design/icons";
import styled from "styled-components";
import { useSelector } from "react-redux";
import usePermission from "../../hooks/usePermission";

const NavBarMobile = ({ isDrawer }) => {
  const history = useHistory();
  const [selected, setSelected] = useState(history.location.pathname.split("/")[1] || "dashboard");
  const { isMobile, collapse, newPost } = useSelector(state => state.general);
  const onClick = (e) => {
    setSelected(e.key);
  };
  const { checkPermission } = usePermission();

  return (
    <SideBarStyled>
      <Menu
        className="menu"
        inlineCollapsed={isMobile ? !isDrawer : collapse}
        onClick={onClick}
        selectedKeys={selected}
        mode="inline"
        theme="light">
        <Menu.Item key="dashboard" icon={<PieChartOutlined />}>Дашбоард <NavLink to="/" /> </Menu.Item>
        {checkPermission(["POST_MANAGER", "ADMIN"]) && <Menu.Item key="post" icon={<MedicineBoxOutlined />}><Space>Эрсдэлийн мэдэгдэл <Badge count={newPost}/></Space><NavLink to="/post" /></Menu.Item>}
        {checkPermission(["USER_MANAGER", "ADMIN"]) && <Menu.Item key="app" icon={<TeamOutlined />}>Апп хэрэглэгч<NavLink to="/app" /></Menu.Item>}
        {checkPermission(["POST_MANAGER", "ADMIN", "USER_MANAGER"]) && <Menu.SubMenu key="settings" title="Тохиргоо" icon={<SettingOutlined />}>
          {checkPermission(["POST_MANAGER", "ADMIN", "USER_MANAGER"]) && <Menu.Item key="Sector" icon={<HomeOutlined />}>Салбар нэгж<NavLink to="/sector" /></Menu.Item>}
          {checkPermission(["ADMIN"]) && <Menu.Item key="staff" icon={<UserOutlined />}>Админ хэрэглэгч<NavLink to="/staff" /></Menu.Item>}
          {checkPermission(["ADMIN"]) && <Menu.Item key="permission" icon={<FormOutlined />}>Хандах эрх<NavLink to="/permission" /></Menu.Item>}
          {checkPermission(["ADMIN"]) && <Menu.Item key="log" icon={<FileProtectOutlined />}>Лог бүртгэл<NavLink to="/log" /></Menu.Item>}
        </Menu.SubMenu>}
      </Menu>
    </SideBarStyled>
  );
};

const SideBarStyled = styled.div`
  height: 100%; 
  overflow-y: scroll;
  .toggle { 
    bottom: 0px;
  }
  .menu {
    height: 100%;
    border: none;
    width: 100%;
  } 
`;

export default NavBarMobile;
