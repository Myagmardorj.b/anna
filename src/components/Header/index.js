import React from "react"; 
import { Layout, Dropdown, Menu, Space, Avatar, Button } from "antd"; 
import styled from "styled-components"; 
import Face from "../../assets/recognition.png";
import { useDispatch } from "react-redux";

const { Header } = Layout;

export default () => {    

  const dispatch = useDispatch();

  const logout = () => {
    dispatch({
      type: "auth.logout",
    });
  };

  return (
    <Container> 
      <Space className="logo" > 
          <Avatar size={55} src={Face} />
          <div className="logo-text">
            <div style={{ color: "#37474F" }}>
              Face
            </div>
            <div style={{ color: "#00B8D4" }}>
              recognition
            </div>
          </div>
      </Space> 
      <Space>
        <Button shape="round" >
          Бүртгүүлэх
        </Button>
        
        <Button onClick={logout} shape="round" type="primary"  >
          Нэвтрэх
        </Button>
      </Space>
    </Container>
  );
};

const Container = styled(Header)`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding-left: 24px;
  padding-right: 24px;
  height: 80px; 
  line-height: normal;
  user-select: none; 

  border-bottom: 1px solid #ededed;
  box-shadow: 0 2px 8px #f0f1f2; 
 
  background: rgb(249,247,255);
  background: linear-gradient(100deg, rgba(249,247,255,1) 0%, rgba(249,247,255,1) 100%);

  .ant-btn {
    border-color: #00B8D4;
    color: #00B8D4;;
  }

  .ant-btn-primary { 
    background: #00B8D4;
    border-color: #00B8D4;
    color: #fff;
  }

  .logo { 
    margin-left: 14px;
    font-weight: 600; 
    color: #333;    
  } 
`;