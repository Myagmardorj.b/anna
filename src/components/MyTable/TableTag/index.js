import React from "react";
import styled from "styled-components";
import styles from "./styles.module.scss";

const TableTag = ({ children, className, ...rest }) => {
  return (
    <Container>
      <span className={`"tag" ${className}`} {...rest}>
        {children}
      </span>
    </Container>
  );
};

const Container = styled.div`
.tag {
  padding: 2px 8px;
  border-radius: 4px;
  background-color: #e3e8ee;
  font-size: 8px;
  line-height: 14px;
  color: #4f566b;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  word-break: keep-all;
  display: inherit;
}
`;

export default TableTag;
