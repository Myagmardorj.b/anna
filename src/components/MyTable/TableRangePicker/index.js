import React from "react";
import moment from "moment";
import { DatePicker } from "formik-antd";
import TableTag from "../TableTag";

const TableRangePicker = ({ name, value, onChange, ...rest }) => {
  const handleDateRange = (range) => {
    let rangeData = [false, false];

    switch (range) {
      case "1hour":
        rangeData = [moment().subtract(1, "hour"), moment()];
        break;
      case "2hour":
        rangeData = [moment().subtract(2, "hour"), moment()];
        break;
      case "1day":
        rangeData = [moment().subtract(1, "day").startOf("day"), moment().endOf("day")];
        break;
      case "2day":
        rangeData = [moment().subtract(2, "day").startOf("day"), moment().endOf("day")];
        break;
      case "week":
        rangeData = [moment().subtract(1, "week").startOf("day"), moment().endOf("day")];
        break;
      case "2week":
        rangeData = [moment().subtract(2, "week").startOf("day"), moment().endOf("day")];
        break;
      case "1month":
        rangeData = [moment().subtract(1, "month").startOf("day"), moment().endOf("day")];
        break;
      default:
        break;
    }

    onChange(rangeData);
  };

  const renderPickerFooter = () => (
    <div className="datepicker-footer">
      <TableTag onClick={() => handleDateRange("1hour")}>Сүүлийн 1 цаг</TableTag>
      <TableTag onClick={() => handleDateRange("2hour")}>Сүүлийн 2 цаг</TableTag>
      <TableTag onClick={() => handleDateRange("1day")}>Сүүлийн 1 хоног</TableTag>
      <TableTag onClick={() => handleDateRange("2day")}>Сүүлийн 2 хоног</TableTag>
      <TableTag onClick={() => handleDateRange("week")}>Сүүлийн долоо хоног</TableTag>
      <TableTag onClick={() => handleDateRange("2week")}>Сүүлийн 2 долоо хоног</TableTag>
      <TableTag onClick={() => handleDateRange("1month")}>Сүүлийн 1 сар</TableTag>
    </div>
  );
  return (
    <DatePicker.RangePicker
      name={name}
      disabledDate={(current) => current && current > moment().endOf("day")}
      allowClear={false}
      format={"YYYY-MM-DD HH:mm"}
      showTime={{
        hideDisabledOptions: true,
        defaultValue       : [moment("00:00:00", "HH:mm"), moment("23:59:59", "HH:mm")],
      }}
      value={[value[0], value[1]]}
      onChange={onChange}
      renderExtraFooter={renderPickerFooter}
      placeholder="Эхлэх огноо..."
      {...rest}
    />
  );
};
export default TableRangePicker;
