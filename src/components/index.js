import PrivateRoute from "./Routes/Private";
import PublicRoute from "./Routes/Public";
import MyTable from "./MyTable";
import NavBar from "./NavBar";
import Header from "./Header";
import ImageUpload from "./ImageUpload";
import VideoUpload from "./VideoUpload";
import MultiImageUpload from "./MultiImageUpload";
import RowAction from "./RowAction";
import * as Design from "./Design";
import Loader from "./Loader";
import NavBarMobile from "./NavBarMobile";

export {
  PrivateRoute,
  PublicRoute,
  NavBar,
  Header,
  MyTable,
  ImageUpload,
  VideoUpload,
  MultiImageUpload,
  RowAction,
  Design,
  Loader,
  NavBarMobile
};
