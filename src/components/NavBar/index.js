import { FileProtectOutlined, FormOutlined, HomeOutlined, LeftOutlined, MedicineBoxOutlined, NotificationOutlined, PieChartOutlined, RightOutlined, SettingOutlined, TeamOutlined, UserOutlined } from "@ant-design/icons";
import { Badge, Menu, Space } from "antd";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink, useHistory } from "react-router-dom";
import styled from "styled-components";
import usePermission from "../../hooks/usePermission";

const NavBar = ({ isDrawer }) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [selected, setSelected] = useState(history.location.pathname.split("/")[1] || "dashboard");
  const { isMobile, collapse, newPost } = useSelector(state => state.general);

  const onClick = (e) => {
    setSelected(e.key);
  };
  const { checkPermission } = usePermission();

  const data = [ {
    label: (
      <div>
        Дашбоард <NavLink to="/" />
      </div>
    ),
    key      : "dashboard",
    className: "item",
    icon     : <PieChartOutlined />,
  }, {
    label: (
      <div>
        <Space>Эрсдэлийн мэдэгдэл <Badge count={newPost}/></Space><NavLink to="/post" />
      </div>
    ),
    key      : "post",
    className: "item",
    icon     : <MedicineBoxOutlined />,
  }
  ];

  return (
    <SideBarStyled>
      <Menu
        // items={data}
        className="menu"
        inlineCollapsed={isMobile ? !isDrawer : collapse}
        onClick={onClick}
        selectedKeys={selected}
        mode="inline"
        theme="light">
        <Menu.Item key="dashboard" icon={<PieChartOutlined />}>Дашбоард <NavLink to="/" /> </Menu.Item>
        {checkPermission(["POST_MANAGER", "ADMIN"]) && <Menu.Item key="post" icon={<MedicineBoxOutlined />}><Space>Эрсдэлийн мэдэгдэл <Badge count={newPost}/></Space><NavLink to="/post" /></Menu.Item>}
        {checkPermission(["USER_MANAGER", "ADMIN"]) && <Menu.Item key="app" icon={<TeamOutlined />}>Апп хэрэглэгч<NavLink to="/app" /></Menu.Item>}
        {checkPermission(["ADMIN"]) && <Menu.Item key="notify" icon={<NotificationOutlined />}>Мэдэгдэл<NavLink to="/notify" /></Menu.Item>}
        {checkPermission(["POST_MANAGER", "ADMIN", "USER_MANAGER"]) && <Menu.SubMenu key="settings" title="Тохиргоо" icon={<SettingOutlined />}>
          {checkPermission(["POST_MANAGER", "ADMIN", "USER_MANAGER"]) && <Menu.Item key="Sector" icon={<HomeOutlined />}>Салбар нэгж<NavLink to="/sector" /></Menu.Item>}
          {checkPermission(["ADMIN"]) && <Menu.Item key="staff" icon={<UserOutlined />}>Админ хэрэглэгч<NavLink to="/staff" /></Menu.Item>}
          {checkPermission(["ADMIN"]) && <Menu.Item key="permission" icon={<FormOutlined />}>Хандах эрх<NavLink to="/permission" /></Menu.Item>}
          {checkPermission(["ADMIN"]) && <Menu.Item key="log" icon={<FileProtectOutlined />}>Лог бүртгэл<NavLink to="/log" /></Menu.Item>}
        </Menu.SubMenu>}
      </Menu>
      <div
        className="toggle"
        onClick={() =>{
          dispatch({
            type   : "general.collapse",
            payload: !collapse
          });
        }}
      >
        {collapse ? <LeftOutlined /> : <RightOutlined />}
      </div>
    </SideBarStyled>
  );
};

const SideBarStyled = styled.div`
  height: calc(100%); 
  overflow-y: scroll;
  overflow-x: none;
  .toggle { 
    background: #FAFAFA;
    height: 45px;
    display: flex;
    align-items: center;
    justify-content: center; 
    cursor: pointer;
    animation: translating 0.5s linear infinite;
    transition: 0.5s ease;
    :hover {
      background: #E6F7FF;
      color: #1890FF;
    }
  }
  .menu {
    height: calc(100% - 45px); 
    border: none;
  }

  @media only screen and (max-width: 390px) {  
    min-width: 100%;
    max-width: 100%;
    
  }
`;

export default NavBar;
