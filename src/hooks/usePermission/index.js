import { useSelector } from "react-redux";

export default () => {
  const { user } = useSelector(state => state.auth);

  return {
    checkPermission: (codes = []) => {
      let allowPermission = false;
      if (codes.length > 0) {
        if (user?.role === "SUPER")
          allowPermission = true;
        else
          codes.forEach(item => {
            if (item === user?.role)
              allowPermission = true;
          });

      }
      return allowPermission;
    }
  };
};