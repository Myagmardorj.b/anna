import useSWR from "swr";
import request from "../utils/request";

const useRate = () => {
  const {
    data: rateData = {
      rates      : [],
      indexedRate: {},
    },
    error,
    isValidating,
    mutate,
  } = useSWR(
    "/api/swap/rate/list",
    async (input) => {
      const res = await request.get(input);
      const gahai = {
        rates      : res.map((item) => ({ key: item.pair, rate: item.rate })),
        indexedRate: res.reduce((accumilator, iterator) => {
          accumilator[iterator.pair] = iterator.rate;
          return accumilator;
        }, {}),
      };
      return gahai;
    },
    {
      revalidateOnFocus: false,
    }
  );

  return {
    data   : rateData,
    calRate: (key, val) => {
      return val * rateData.indexedRate[key];
    },
    error,
    isValidating,
    mutate,
  };
};

export default useRate;
