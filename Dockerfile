FROM node:16-alpine AS builder
WORKDIR /usr/src/app
COPY ./package* ./
RUN npm install
COPY . .
RUN npm run build

FROM nginx:1.17
WORKDIR /var/www
COPY --from=builder /usr/src/app/build ./
COPY deployment/nginx.conf /etc/nginx/nginx.conf
EXPOSE 80 443

ENTRYPOINT ["nginx"]
CMD ["-g", "daemon off;"]